import numpy as np

def mymatmat(A, B):

    C = np.zeros((A.shape[0], B.shape[1]), dtype=A.dtype)

    for i in range(A.shape[0]):
        for j in range(B.shape[1]):
            cij = 0
            for k in range(A.shape[1]):

                cij += A[i, k]*B[k, j]

            C[i, j] = cij

    return C


A = np.array([[1, 2, 3],
              [4, 5, 6],
              [7, 8, 9]], dtype=np.float64)

Cref = A.dot(A)

C = mymatmat(A, A)

print("diff: ", np.sum(abs(Cref - C)))
print(C)
