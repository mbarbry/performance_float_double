import numpy as np
import matplotlib.pyplot as plt


N_mat = np.array([100, 500, 1000], dtype=int)

Nrepeat = 100

h = 9
w = 16*h/9
ft = 20

fig = plt.figure(1, figsize=(w, h))

for imat, mat_size in enumerate(N_mat):

    ax = fig.add_subplot(1, N_mat.size, imat+1)

    print("Mat size: ", mat_size**2)
    data_float = np.loadtxt("test_float_{}_{}.txt".format(mat_size, Nrepeat))
    data_double = np.loadtxt("test_double_{}_{}.txt".format(mat_size, Nrepeat))
    ax.boxplot([data_float, data_double])
    print("float: total={0:.6e}, mean={1:.6e}, std={2:.6e}".format(data_float.sum(),
                                                                   data_float.mean(),
                                                                   data_float.std()))
    print("double: total={0:.6e}, mean={1:.6e}, std={2:.6e}".format(data_double.sum(),
                                                                    data_double.mean(),
                                                                    data_double.std()))

    ax.set_title("Matrix size: {}".format(mat_size**2), fontsize=ft)
    ax.set_xticklabels(["float", "double"])
    ax.set_ylabel("Time spend", fontsize=ft)

fig.tight_layout()
fig.savefig("test_time.svg", format="svg")
plt.show()
