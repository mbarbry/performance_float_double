#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

void mymatmat(float *A, int NrowA, int NcolA, float *B, int NcolB, float *C)
{

  int i, j, k, a_index, b_index, c_index;
  float cij;

  for (i = 0; i < NrowA; i++)
  {
    for (j = 0; j < NcolB; j++)
    {
      cij = 0.0;
      for (k = 0; k < NcolA; k++)
      {
        // column major
        a_index = i + k*NrowA;
        // row major
        b_index = k + j*NcolA;
        cij += A[a_index]*B[b_index];
      }
      // row major
      c_index = i*NcolB + j;
      C[c_index] = cij;
    }
  }

}

void dmymatmat(double *A, int NrowA, int NcolA, double *B, int NcolB, double *C)
{

  int i, j, k, a_index, b_index, c_index;
  double cij;

  for (i = 0; i < NrowA; i++)
  {
    for (j = 0; j < NcolB; j++)
    {
      cij = 0.0;
      for (k = 0; k < NcolA; k++)
      {
        // column major
        a_index = i + k*NrowA;
        // row major
        b_index = k + j*NcolA;
        cij += A[a_index]*B[b_index];
      }
      // row major
      c_index = i*NcolB + j;
      C[c_index] = cij;
    }
  }

}


void ref_calc()
{

  int N = 3, i, j;
  float A[N*N], B[N*N], C[N*N];
  float Cref[9] = {30, 36, 42, 66, 81, 96, 102, 126, 150};
  float diff = 0.0;

  printf("Reference test\n");
  // initialize reference vals
  printf("Cref = ");
  for (i=0; i < N; i++)
  {
    for (j=0; j < N; j++)
    {
      A[i + j*N] = i*N + j + 1;
      B[i + j*N] = i*N + j + 1;
      C[i + j*N] = 0;
      printf("%f %d    ", Cref[j + i*N], j + i*N);
    }
    printf("\n       ");
  }

  printf("Hola!\n");

  mymatmat(A, N, N, B, N, C);

  printf("C = ");
  for (i=0; i < N; i++)
  {
    for (j=0; j < N; j++)
    {
      printf("%f  ", C[i*N + j]);
      diff += abs(C[i*N + j] - Cref[i*N + j]);
    }
    printf("\n    ");
  }
  printf("\ndiff = %f\n", diff);

}

void generate_random_array(float *arr, int N, float max_number, float minimum_number)
{

  int i;
  float scale;

  for (i=0; i< N; i++)
  {
    scale = rand() / (float) RAND_MAX; /* [0, 1.0] */
    arr[i] = minimum_number + scale*(max_number - minimum_number);
  }

}

void dgenerate_random_array(double *arr, int N, double max_number, double minimum_number)
{

  int i;
  double scale;

  for (i=0; i< N; i++)
  {
    scale = rand() / (double) RAND_MAX; /* [0, 1.0] */
    arr[i] = minimum_number + scale*(max_number - minimum_number);
  }

}


void print_matrix(float *arr, int Nrow, int Ncol)
{

  int i, j;

  printf("arr = ");
  for (i = 0; i < Nrow; i++)
  {
    for (j=0; j < Ncol; j++)
    {
      printf("%f  ", arr[i*Ncol + j]);
    }
    printf("\n      ");
  }
}

void test_function_float(int N, int Nrepeat, char *fname)
{

  float *arr1, *arr2, *arr_res;
  double *time_spend;
  double time_ite;
  int irepeat;
  struct timeval stop, start;

  printf("Enter test_function_float\n");

  arr1 = (float*) malloc(N*N*sizeof(float));
  arr2 = (float*) malloc(N*N*sizeof(float));
  arr_res = (float*) malloc(N*N*sizeof(float));
  time_spend = (double*) malloc(Nrepeat*sizeof(double));

  for (irepeat = 0; irepeat < Nrepeat; irepeat++)
  {
    generate_random_array(arr1, N*N, 0, 100);
    generate_random_array(arr2, N*N, 0, 100);

    gettimeofday(&start, NULL);
    mymatmat(arr1, N, N, arr2, N, arr_res);
    gettimeofday(&stop, NULL);

    // time in microseconds
    time_ite = (stop.tv_sec - start.tv_sec)*1000000 + stop.tv_usec - start.tv_usec;
    time_spend[irepeat] = time_ite*1.0e-6;
    //printf("%d: time = %.6e\n", irepeat, time_spend[irepeat]);

  }

  printf("Write data to: %s\n", fname);
  // write data to file
  FILE *fp;

  fp = fopen(fname, "w");

  for (irepeat = 0; irepeat < Nrepeat; irepeat++)
  {
    fprintf(fp, "%.6e\n", time_spend[irepeat]);
  }

  fclose(fp);

  free(arr1);
  free(arr2);
  free(arr_res);
  free(time_spend);
}


void test_function_double(int N, int Nrepeat, char *fname)
{

  double *arr1, *arr2, *arr_res;
  double *time_spend;
  double time_ite;
  int irepeat;
  struct timeval stop, start;

  printf("Enter test_function_double\n");

  arr1 = (double*) malloc(N*N*sizeof(double));
  arr2 = (double*) malloc(N*N*sizeof(double));
  arr_res = (double*) malloc(N*N*sizeof(double));
  time_spend = (double*) malloc(Nrepeat*sizeof(double));

  for (irepeat = 0; irepeat < Nrepeat; irepeat++)
  {
    dgenerate_random_array(arr1, N*N, 0, 100);
    dgenerate_random_array(arr2, N*N, 0, 100);

    gettimeofday(&start, NULL);
    dmymatmat(arr1, N, N, arr2, N, arr_res);
    gettimeofday(&stop, NULL);

    // time in microseconds
    time_ite = (stop.tv_sec - start.tv_sec)*1000000 + stop.tv_usec - start.tv_usec;
    time_spend[irepeat] = time_ite*1.0e-6;
    //printf("%d: time = %.6e\n", irepeat, time_spend[irepeat]);

  }

  printf("Write data to: %s\n", fname);
  // write data to file
  FILE *fp;

  fp = fopen(fname, "w");

  for (irepeat = 0; irepeat < Nrepeat; irepeat++)
  {
    fprintf(fp, "%.6e\n", time_spend[irepeat]);
  }

  fclose(fp);

  free(arr1);
  free(arr2);
  free(arr_res);
  free(time_spend);
}


int main(void)
{

  int N;
  int Nrepeat = 100;

  ref_calc();

  N = 100;
  test_function_float(N, Nrepeat, "test_float_100_100.txt");
  test_function_double(N, Nrepeat, "test_double_100_100.txt");

  N = 500;
  test_function_float(N, Nrepeat, "test_float_500_100.txt");
  test_function_double(N, Nrepeat, "test_double_500_100.txt");

  N = 1000;
  test_function_float(N, Nrepeat, "test_float_1000_100.txt");
  test_function_double(N, Nrepeat, "test_double_1000_100.txt");

  N = 5000;
  test_function_float(N, Nrepeat, "test_float_5000_100.txt");
  test_function_double(N, Nrepeat, "test_double_5000_100.txt");


  return 0;

}
