# Performance_float_double

A small code to compare performance gain of using float over double.

Be aware that using float comes at the cost of accuracy, which can be crucial to
your algorithm.

In this demonstration, we will use a naive implmentation of matrix matrix multiplication.

## Results

Small tests were perform on an AMD Ryzen 5 2600 Six-Core Processor.
No parallelization were used.

| Matrix size | float mean    | double mean   | speed up using float | float std     | double std   |
|-------------|---------------|---------------|----------------------|---------------|--------------|
| 10000       | 3.109280e-03  | 3.099160e-03  | -0.3%                | 1.242592e-04  | 4.206471e-06 |
| 250000      | 3.778996e-01  | 3.784194e-01  |  0.1%                | 1.179814e-03  | 3.738654e-04 |
| 1000000     | 3.015917e+00  | 3.052188e+00  |  1.2%                | 1.237782e-02  | 6.444060e-03 |


![test](src/test_time.svg)


## Conclusions

* For small matrix sizes, double calculations is actually faster than float.
* For medium size matrices, the difference is almost null.
* For large size matrices, float calculation start to be fast (1.2%), but the gain
remain marginal.
